﻿var animationDuration = 3000;
var animationInterval = 40;

var AlarmManager = {
	init: function(date, list_element)
		{
			this.date = date;
			this.alarms = [];
			this.list = list_element;
		}
	,
	checkAlarms:
		function()
		{
			for(var i in this.alarms)
			{
				var al = this.alarms[i];
				if(al.check(this.date))
				{
					var buttons = [
						{
							text: "Zamknij",
							onclick: function()
								{
									snd.pause();
									box.close();
									clearInterval(moveInterval);
								}
						}
					];
					
					if(al.napTime > 0)
					{
						buttons.push(
							{
								text: "Drzemka (" + al.napTime + " min)",
								onclick: function()
									{
										box.close();
										clearInterval(moveInterval);
										al.nap();
									}
							}
						);
					}
					
					var box = new AlertBox("Alarm!!!", al.message, buttons);
					box.angle = 0;
					box.box.onmouseenter = function()
						{
							this.mouseEntered = true;
						}
					;
					
					box.box.onmouseleave = function()
						{
							this.mouseEntered = false;
						}
					;
					
					var radius = 100;
					
					box.display(
						"(window.innerWidth / 2) + " + radius + " - (ob.box.clientWidth / 2)",
						"(window.innerHeight / 2) - (ob.box.clientHeight / 2)",
						false
					);
					
					var moveInterval = setInterval(
						function()
						{
							if(!box.box.mouseEntered)
							{
								// Add angle step
								box.angle += animationInterval / animationDuration * 360;
								if(box.angle > 360)
									box.angle -= 360;
								
								boxX = (window.innerWidth / 2) + Math.cos(Math.PI / 180 * box.angle) * radius - (box.box.clientWidth / 2);
								boxY = (window.innerHeight / 2) + Math.sin(Math.PI / 180 * box.angle) * radius - (box.box.clientHeight / 2);
								
								box.box.style.top = boxY + "px";
								box.box.style.left = boxX + "px";
							}
						},
						animationInterval
					);
					
					var snd = new Audio("alarm.mp3");
					snd.loop = true;
					snd.play();
				}
			}
		}
	,
	addAlarm: function(alarm)
		{
			this.alarms.push(alarm);
			alarm.element.classList.add("slideHidden");
			this.list.appendChild(alarm.element);
			
			var noAlarms = document.getElementById("noAlarms");
			
			window.requestAnimationFrame(
				function()
				{
					noAlarms.classList.toggle("slideShown", false);
					noAlarms.classList.toggle("slideHidden", true);
					
					window.requestAnimationFrame(
						function()
						{
							alarm.element.classList.add("slide");
							alarm.element.classList.remove("slideHidden");
						}
					);
				}
			);
		}
	,
	removeAlarm: function(alarm)
		{
			alarm.element.classList.add("slideHidden");
			
			setTimeout(
				function()
				{
					AlarmManager.list.removeChild(alarm.element);
				}, 510
			);
			this.alarms.splice(this.alarms.indexOf(alarm), 1);
			
			if(this.alarms.length <= 0)
			{
				var noAlarms = document.getElementById("noAlarms");
				noAlarms.classList.add("slideShown");
				noAlarms.classList.remove("slideHidden");
			}
		}
};

function Alarm(msg, y, m, d, h, min, naptime, rpt)
{
	console.log(arguments);
	var message, setDate, napTime, repeating;
	var ob = this;
	
	this.element = document.createElement("div");
	this.element.alarm = this;
	this.element.className = "alarm";
	
	var spans = {
		message:
			{
				title: "Tekst wiadomości",
				form: function()
					{
						var input = document.createElement("input");
						input.type = "text";
						input.name = "message";
						input.value = this.message;
						return input;
					}
				,
				edit: function(form)
					{
						this.message = form.message.value;
					}
			}
		,
		date:
			{
				title: "Data alarmu",
				form: function()
					{
						var EditCalendar;
						var div = document.createElement("div");
						div.classList.add("head");
						var left = document.createElement("input");
						left.type = "button";
						left.value = "<";
						left.onclick = function()
							{
								EditCalendar.prevMonth();
							}
						;
						
						var right = document.createElement("input");
						right.type = "button";
						right.value = ">";
						right.onclick = function()
							{
								EditCalendar.nextMonth();
							}
						;
						
						div.appendChild(left);
						div.appendChild(document.createElement("span"));
						div.appendChild(right);
						
						var year = document.createElement("input");
						year.type = "hidden";
						year.name = "year";
						var month = document.createElement("input");
						month.type = "hidden";
						month.name = "month";
						var day = document.createElement("input");
						day.type = "hidden";
						day.name = "day";
						
						var tableDisplay = document.createElement("div");
						tableDisplay.classList.add("display");
						var mainDiv = document.createElement("div");
						mainDiv.className = "kalendarz";
						mainDiv.style.textAlign = "center";
						mainDiv.appendChild(div);
						mainDiv.appendChild(tableDisplay);
						mainDiv.appendChild(year);
						mainDiv.appendChild(month);
						mainDiv.appendChild(day);
						
						EditCalendar = new _Calendar(tableDisplay, new Date(), null);
						EditCalendar.init();
						EditCalendar.changeDate = function()
							{
								year.value = this.selectedDate.getFullYear();
								month.value = this.selectedDate.getMonth();
								day.value = this.selectedDate.getDate();
							}
						;
						EditCalendar.changeDate();
						EditCalendar.updateCalendar();
						
						return mainDiv;
					}
				,
				edit: function(form)
					{
						var newdate = new Date(this.setDate);
						newdate.setFullYear(parseInt(form.year.value));
						newdate.setMonth(parseInt(form.month.value));
						newdate.setDate(parseInt(form.day.value));
						this.setDate = newdate;
					}
			}
		,
		time:
			{
				title: "Godzina alarmu",
				form: function()
					{
						var frag = document.createDocumentFragment();
						var h = document.createElement("input");
						h.name = "hours";
						h.type = "text";
						h.className = "time";
						h.maxLength = 2;
						h.value = this.setDate.getHours();
						h.value = h.value < 10 ? "0" + h.value : h.value;
						
						var m = document.createElement("input");
						m.name = "minutes";
						m.type = "text";
						m.className = "time";
						m.maxLength = 2;
						m.value = this.setDate.getMinutes();
						m.value = m.value < 10 ? "0" + m.value : m.value;
						
						frag.appendChild(h);
						frag.appendChild(document.createTextNode(" : "));
						frag.appendChild(m);
						
						return frag;
					}
				,
				edit: function(form)
					{
						var hours = parseInt(form.hours.value);
						var minutes = parseInt(form.minutes.value);
						var newdate = new Date(this.setDate);
						newdate.setHours(isNaN(hours) ? 0 : hours);
						newdate.setMinutes(isNaN(minutes) ? 0 : minutes);
						this.setDate = newdate;
					}
			}
		,
		napTime:
			{
				title: "Czas trwania drzemki",
				form: function()
					{
						var input = document.createElement("input");
						input.type = "text";
						input.name = "naptime";
						input.value = this.napTime;
						return input;
					}
				,
				edit: function(form)
					{
						this.napTime = parseInt(form.naptime.value);
					}
			}
		,
		repeating:
			{
				title: "Powtarzanie",
				form: function()
					{
						var table = document.createElement("table");
						var tr = document.createElement("tr");
						var td = document.createElement("td");
						td.colSpan = 2;
						td.textContent = "Powtarzaj w te dni:";
						tr.appendChild(td);
						table.appendChild(tr);
						
						for(var i in Calendar.dayNames)
						{
							tr = document.createElement("tr");
							td = document.createElement("td");
							td.innerHTML = Calendar.dayNames[i];
							tr.appendChild(td);
							td = document.createElement("td");
							var checkbox = document.createElement("input");
							checkbox.name = "repeat";
							checkbox.type = "checkbox";
							checkbox.checked = this.repeating.indexOf(parseInt(i)) >= 0;
							checkbox.value = i;
							td.appendChild(checkbox);
							tr.appendChild(td);
							table.appendChild(tr);
						}
						var div = document.createElement("div");
						div.className = "repeatingSlide";
						div.appendChild(table);
						
						return div;
					}
				,
				edit: function(form)
					{
						var repeatDays = [];
						console.log(form);
						for(var i in form['repeat'])
						{
							if(form['repeat'][i].checked)
								repeatDays.push(parseInt(form['repeat'][i].value));
						}
						this.repeating = repeatDays;
					}
			}
	};
	
	for(var n in spans)
	{
		var span = document.createElement("span");
		span.className = n;
		span.prop = spans[n];
		span.ondblclick = function()
			{
				//console.log(this.prop);
				var ownSpan = this;
				var box = new AlertBox(this.prop.title, "", [
					{
						text: "Anuluj",
						onclick: function()
							{
								this.alertbox.close();
							}
					},
					{
						text: "Zatwierdź",
						onclick: function()
							{
								ownSpan.prop.edit.call(ob, this.form);
								ownSpan.classList.toggle("bgColor", false);
								ownSpan.classList.add("notify");
								window.requestAnimationFrame(
									function()
									{
										ownSpan.classList.toggle("bgColor");
										ownSpan.classList.remove("notify");
									}
								);
								this.alertbox.close();
							}
					}
				]);
				console.log(box);
				box.formBox();
				box.messageElement.appendChild(this.prop.form.call(ob));
				box.display("center", "center", true);
			}
		;
		this.element.appendChild(span);
	}
	
	var deleteSpan = document.createElement("span");
	deleteSpan.className = "delete";
	deleteSpan.onclick = function()
		{
			var box = new AlertBox("Usuń alarm", "Czy jesteś pewien że chcesz usunąć ten alarm? Tej operacji nie da się cofnąć!", [
				{
					text: "Anuluj",
					onclick: function()
						{
							this.alertbox.close();
						}
				},
				{
					text: "Usuń",
					onclick: function()
						{
							this.alertbox.close();
							AlarmManager.removeAlarm(ob);
						}
				}
			]);
			
			box.display("center", "center", true);
		}
	;
	
	this.element.appendChild(deleteSpan);
		
	Object.defineProperties(this,
		{
			message:
				{
					configurable: false,
					enumerable: true,
					get: function()
						{
							return message;
						}
					,
					set: function(val)
						{
							message = val;
							this.element.children[spans.keys().indexOf("message")].innerHTML = message;
						}
				}
			,
			setDate:
				{
					configurable: false,
					enumerable: true,
					get: function()
						{
							return setDate;
						}
					,
					set: function(val)
						{
							setDate = val;
							if(this.repeating && this.repeating.length > 0)
							{
								this.element.children[spans.keys().indexOf("date")].innerHTML = "Data: --";
							}
							else
							{
								this.element.children[spans.keys().indexOf("date")].innerHTML = "Data: " + this.setDate.getDate() + " " + Calendar.monthNames[this.setDate.getMonth()] + " " + this.setDate.getFullYear() + "r.";
							}
							
							this.element.children[spans.keys().indexOf("time")].innerHTML = "Godzina: " + (this.setDate.getHours() < 10 ? "0" : "") + this.setDate.getHours() + ":" + (this.setDate.getMinutes() < 10 ? "0" : "") + this.setDate.getMinutes();
						}
				}
			,
			napTime:
				{
					configurable: false,
					enumerable: true,
					get: function()
						{
							return napTime;
						}
					,
					set: function(val)
						{
							napTime = val || 0;
							
							if(napTime <= 0)
							{
								this.element.children[spans.keys().indexOf("napTime")].innerHTML = "Czas drzemki: Nie ustawiona";
							}
							else
							{
								this.element.children[spans.keys().indexOf("napTime")].innerHTML = "Czas drzemki: " + napTime + " min";
							}
						}
				}
			,
			repeating:
				{
					configurable: false,
					enumerable: true,
					get: function()
						{
							return repeating;
						}
					,
					set: function(val)
						{
							repeating = val;
							var repeatSpan = this.element.children[spans.keys().indexOf("repeating")];
							var dateSpan = this.element.children[spans.keys().indexOf("date")];
							if(!repeating || repeating.length <= 0)
							{
								repeatSpan.innerHTML = "Powtarzanie: --";
								dateSpan.innerHTML = "Data: " + this.setDate.getDate() + " " + Calendar.monthNames[this.setDate.getMonth()] + " " + this.setDate.getFullYear() + "r.";
							}
							else
							{
								var ret = "";
								for(var i in this.repeating)
								{
									ret += Calendar.dayNames[this.repeating[i]] + ", ";
								}
								
								repeatSpan.innerHTML = "Powtarzanie: " + ret.slice(0, -2);
								dateSpan.innerHTML = "Data: --";
							}
						}
				}
		}
	);
	
	this.message = msg;
	this.setDate = new Date(y, m, d, h, min);
	this.napTime = naptime || 0;
	this.repeating = rpt || [];
	this.napDate = new Date(this.setDate);
	this.napOn = false;
	this.nextCheck = new Date(this.setDate);
	
	this.check = function(date)
		{
			if(this.napOn && this.napDate < date)
			{
				this.napOn = false;
				return true;
			}
			
			console.log('hi');
			//console.log(this);
			if(
				(this.repeating.length <= 0 && this.nextCheck.valueOf() == this.setDate.valueOf() && this.setDate < date) ||
				(this.repeating.length > 0 && this.nextCheck < date && this.repeating.indexOf(date.getDDay()) != -1 && this.setDate.getHours() == date.getHours() && this.setDate.getMinutes() == date.getMinutes())
			)
			{
				this.nextCheck.setHours(this.nextCheck.getHours() + 24);
				return true;
			}
			
			return false;
		}
	;
	
	this.nap = function()
		{
			this.napOn = true;
			this.napDate = new Date();
			this.napDate.setMinutes(this.napDate.getMinutes() + this.napTime);
		}
	;
}