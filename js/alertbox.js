﻿function AlertBox(title, message, buttons)
{
	this.overlay = document.getElementById("black-overlay");
	if(!this.overlay)
	{
		this.overlay = document.createElement("div");
		this.overlay.id = "black-overlay";
		this.overlay.className = "fade";
		this.overlay.style.position = "fixed";
		this.overlay.style.zIndex = "10000";
		this.overlay.style.width = "100%";
		this.overlay.style.height = "100%";
		this.overlay.style.backgroundColor = "black";
		this.overlay.style.opacity = "0.0";
		this.overlay.style.top = this.overlay.style.left = "0px";
		this.overlay.style.display = "none";
		document.body.appendChild(this.overlay);
	}
	
	this.box = document.createElement("div");
	this.box.classList.add("alertbox-window");
	this.box.alertbox = this;
	this.box.style.display = "none";
	
	this.titleElement = document.createElement("p");
	this.titleElement.className = "title";
	this.titleElement.appendChild(document.createTextNode(title));
	this.box.appendChild(this.titleElement);
	
	this.messageElement = document.createElement("p");
	this.messageElement.className = "message";
	this.messageElement.appendChild(document.createTextNode(message));
	this.box.appendChild(this.messageElement);
	
	this.buttons = [];
	this.buttonsElement = document.createElement("p");
	this.buttonsElement.className = "buttons";
	for(var i in buttons)
	{
		var button = document.createElement("input");
		button.alertbox = this;
		button.type = "button";
		button.value = buttons[i]['text'];
		button.onclick = buttons[i]['onclick'];
		this.buttons.push(button);
		this.buttonsElement.appendChild(button);
	}
	this.box.appendChild(this.buttonsElement);
	document.body.appendChild(this.box);
	
	this.formBox = function()
		{
			var form = document.createElement("form");
			form.appendChild(this.titleElement);
			form.appendChild(this.messageElement);
			form.appendChild(this.buttonsElement);
			this.box.appendChild(form);
		}
	;
	
	this.display = function(x, y, withSlide)
		{
			console.log(arguments);
			var top = y, left = x;
			this.box.style.display = "block";
			if(x == "center")
			{
				left = (window.innerWidth / 2) - (this.box.clientWidth / 2);
			}
			
			if(y == "center")
			{
				top = (window.innerHeight / 2) - (this.box.clientHeight / 2);
			}
			
			var ob = this;
			top = eval(top);
			left = eval(left);
			
			if(withSlide)
			{
				destinedTop = top;
				top = -this.box.clientHeight;
			}
			
			window.requestAnimationFrame(
				function()
				{
					this.overlay.style.display = "block";
					window.requestAnimationFrame(
						function()
						{
							this.overlay.style.opacity = "0.9";
						}.bind(this)
					);
					
					console.log(left);
					this.box.style.top = top + "px";
					this.box.style.left = left + "px";
					
					if(withSlide)
					{
						window.requestAnimationFrame(
							function()
							{
								this.box.classList.add("rotateDown");
								this.box.style.top = destinedTop + "px";
							}.bind(this)
						);
					}
				}.bind(this)
			);
		}
	;
	
	this.close = function()
		{
			document.body.removeChild(this.box);
			if(document.getElementsByClassName("alertbox-window").length <= 0)
			{
				this.overlay.style.opacity = "0.0";
				setTimeout(
					function()
					{
						this.overlay.style.display = "none";
					}.bind(this), 510
				);
			}
		}
	;
}