﻿function _Calendar(tableDisplay, date, display)
{
	this.display = display;
	this.tableDisplay = tableDisplay;
	this.actualDate = date;
	this.selectedDate = new Date(date);
	this.notes = [];
	this.activeNote = null;
	
	
	this.dayNames = [
		"Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota", "Niedziela"
	];
	
	this.dayShortNames = [
		"Pn", "Wt", "Śr", "Cz", "Pt", "So", "Nd"
	];
	
	this.monthNames = [
		"Styczeń", "Luty", "Marzec", "Kwiecień",
		"Maj", "Czerwiec", "Lipiec", "Sierpień",
		"Wrzesień", "Październik", "Listopad", "Grudzień"
	];
	
	this.init = function()
		{
			this.createTable();
		}
	;
	
	// Create table based on selectedDate and activeTable
	this.createTable = function(forDate)
		{
			var date = forDate || this.selectedDate;
			// Create New Span
			var span = document.createElement("span");
			span.dataset['yearmonth'] = date.getFullYear() + "" + date.getMonthh();
			span.appendChild(document.createTextNode(this.monthNames[date.getMonth()] + " " + date.getFullYear()));
			
			// Create Calendar Table
			var ob = this;
			var table = document.createElement("table");
			table.dataset['yearmonth'] = date.getFullYear() + "" + date.getMonthh();
			
			// Decide whether this table is next or previous ( or first one ! )
			var activeTable = this.tableDisplay.querySelector("table.active");
			if(!activeTable)
			{
				// K im firs
				span.classList.add("active");
				table.classList.add("active");
			}
			else
			{
				var activeYearMonth = parseInt(activeTable.dataset['yearmonth']);
				var newYearMonth = parseInt(table.dataset['yearmonth']);
				
				if(activeYearMonth < newYearMonth)
				{
					span.classList.add("next");
					table.classList.add("next");
				}
				else
				{
					span.classList.add("previous");
					table.classList.add("previous");
				}
			}
			
			span.classList.add("leftTransition");
			table.classList.add("leftTransition");
			this.tableDisplay.previousElementSibling.children[1].appendChild(span);
			
			var tr = document.createElement("tr");
			for(var i = 0; i < this.dayShortNames.length; i++)
			{
				var th = document.createElement("th");
				th.appendChild(document.createTextNode(this.dayShortNames[i]));
				tr.appendChild(th);
			}
			table.appendChild(tr);
			
			var days = this.getDaysOfMonth(date);
			tr = document.createElement("tr");
			for(var i = 0; i < 42; i++)
			{
				if(i > 0 && i % 7 == 0)
				{
					table.appendChild(tr);
					tr = document.createElement("tr");
				}
				var td = document.createElement("td");
				td.textContent = days[i][0];
				td.dataset['day'] = days[i][0];
				td.dataset['month'] = days[i][1];
				td.dataset['year'] = days[i][2];
				
				if(days[i][1] != date.getMonth())
					td.classList.add("another-month");
				// If u are active one ... ;>
				else if(days[i][0] == this.selectedDate.getDate() && days[i][1] == this.selectedDate.getMonth() && days[i][2] == this.selectedDate.getFullYear())
					td.classList.add("selected");
				
				if(days[i][0] == this.actualDate.getDate() && days[i][1] == this.actualDate.getMonth() && days[i][2] == this.actualDate.getFullYear())
					td.classList.add("actual");
				
				td.onclick = function() { ob.selectDate(this); };
				td.ondblclick = function() { ob.chooseDate(this); };
				
				td.onmouseover = function()
					{
						if(ob.activeNote)
						{
							ob.tableDisplay.parentElement.removeChild(ob.activeNote);
							delete ob.activeNote;
						}
						
						var note = document.createElement("div");
						note.className = "note";
						note.style.display = "none";
						note.inCell = true;
						
						// Lookup for Note
						for(var n in ob.notes)
						{
							if(ob.notes[n].year == this.dataset['year'] && ob.notes[n].month == this.dataset['month'] && ob.notes[n].day == this.dataset['day'])
							{
								var added = document.createElement("div");
								added.className = "date";
								added.appendChild(document.createTextNode("Dodano: " + this.dataset['year'] + "." + this.dataset['month'] + "." + this.dataset['day']));
								var text = document.createElement("div");
								text.className = "text";
								text.appendChild(document.createTextNode(ob.notes[n].text));
								note.appendChild(added);
								note.appendChild(text);
							}
						}
						
						if(note.getElementsByClassName("date").length <= 0)
						{
							var p = document.createElement("p");
							p.className = "noNotes";
							p.appendChild(document.createTextNode("Brak notatek!"));
							note.appendChild(p);
						}
						
						var add = document.createElement("input");
						add.type = "button";
						add.value = "Dodaj notatke";
						add.onclick = function()
							{
								var temp = this;
								var box = new AlertBox([
										"Dodaj notatke na dzien: \n",
										this.dataset['year'],
										".",
										this.dataset['month'],
										".",
										this.dataset['day']
									].join(""), "", [
										{
											text: "Anuluj",
											onclick: function()
												{
													box.close();
												}
										},
										{
											text: "Dodaj",
											onclick: function()
												{
													ob.notes.push(new Note(temp.dataset['year'], temp.dataset['month'], temp.dataset['day'], this.form.note.value));
													box.close();
												}
										}
									]);
								
								box.titleElement.innerHTML = box.titleElement.innerHTML.replace("\n", "<br>");
								
								var input = document.createElement("input");
								input.type = "text";
								input.name = "note";
								input.placeholder = "Wpisz notatke";
								box.messageElement.appendChild(input);
								
								box.formBox();
								box.display("center", "center", true);
							}.bind(this)
						;
						note.appendChild(add);
						
						ob.activeNote = note;
						note.onmouseover = function()
							{
								ob.activeNote.inSelf = true;
							}
						;
						note.onmouseout = function()
							{
								ob.activeNote.inSelf = false;
							}
						;
						ob.tableDisplay.parentElement.appendChild(note);
						
						// K we added it so now format it position
						// .kalendarz
						// -- .display
						// -- -- table
						// -- -- -- tr
						// -- -- -- -- td <- this
						
						note.style.display = "";
						note.style.top = (
							this.offsetTop
							// table.offsetTop
							+ this.parentElement.parentElement.offsetTop
							// .display.offsetTop
							+ this.parentElement.parentElement.parentElement.offsetTop
							+ this.offsetHeight / 2
						) + "px";
						
						note.style.left = (
							this.offsetLeft
							// table.offsetLeft
							+ this.parentElement.parentElement.offsetLeft
							// .display.offsetLeft
							+ this.parentElement.parentElement.parentElement.offsetLeft
							+ this.offsetWidth / 2
						) + "px";
					}
				;
				
				td.onmouseout = function()
					{
						ob.activeNote.inCell = false;
					}
				;
				
				document.onmousemove = function()
					{
						//console.log(ob);
						if(ob.activeNote && !ob.activeNote.inCell && !ob.activeNote.inSelf)
						{
							ob.tableDisplay.parentElement.removeChild(ob.activeNote);
							delete ob.activeNote;
						}
					}
				;
				//td.appendChild(note);
				tr.appendChild(td);
			}
			table.appendChild(tr);
			this.tableDisplay.appendChild(table);
			
			return table;
		}
	;
	
	this.getDaysOfMonth = function(date)
		{
			var arr = [];
			arr.length = 42; // fixed size
			
			var temp = new Date(date);
			temp.setDate(1);
			
			var startsAt = temp.getDDay();
			
			for(var i = startsAt; i >= 0; i--)
			{
				arr[i] = [ temp.getDate(), temp.getMonth(), temp.getFullYear() ];
				temp.setDate(arr[i][0] - 1);
			}
			
			temp = new Date(date)
			temp.setDate(1);
			
			for(var i = startsAt; i < arr.length; i++)
			{
				arr[i] = [ temp.getDate(), temp.getMonth(), temp.getFullYear() ];
				temp.setDate(arr[i][0] + 1);
			}
			return arr;
		}
	;
	
	this.nextMonth = function()
		{
			this.selectedDate.setMonth(this.selectedDate.getMonth() + 1);
			this.updateCalendar();
		}
	;
	
	this.prevMonth = function()
		{
			this.selectedDate.setMonth(this.selectedDate.getMonth() - 1);
			this.updateCalendar();
		}
	;
	
	this.changeDate = function() {};
	
	this.selectDate = function(cell)
		{
			if  (
					cell.dataset['year'] != undefined && cell.dataset['year'] != null &&
					cell.dataset['month'] != undefined && cell.dataset['month'] != null &&
					cell.dataset['day'] != undefined && cell.dataset['day'] != null
				)
			{
				if 	(
						this.selectedDate.getFullYear() != cell.dataset['year'] ||
						this.selectedDate.getMonth() != cell.dataset['month'] ||
						this.selectedDate.getDate() != cell.dataset['day']
					)
				{
					this.selectedDate.setFullYear(cell.dataset['year']);
					this.selectedDate.setMonth(cell.dataset['month']);
					this.selectedDate.setDate(cell.dataset['day']);
					this.changeDate();
					this.updateCalendar();
				}
			}
		}
	;
	
	this.chooseDate = function()
		{
			if 	(
					this.actualDate.getFullYear() != this.selectedDate.getFullYear() ||
					this.actualDate.getMonth() != this.selectedDate.getMonth() ||
					this.actualDate.getDate() != this.selectedDate.getDate()
				)
			{
				this.actualDate.setFullYear(this.selectedDate.getFullYear());
				this.actualDate.setMonth(this.selectedDate.getMonth());
				this.actualDate.setDate(this.selectedDate.getDate());
				this.updateCalendar();
				this.updateDisplay();
				
				var ob = this.display;
				window.requestAnimationFrame(
					function()
					{
						ob.classList.remove("bgColor");
						ob.classList.add("notify");
						window.requestAnimationFrame(
							function()
							{
								ob.classList.add("bgColor");
								ob.classList.remove("notify");
							}
						);
					}
				);
			}
		}
	;
	
	this.updateCalendar = function()
		{
			// div.head <- previousElementSibling
			// -- input
			// -- span <- children[1]
			// -- input
			// div.display <- this.tableDisplay
			// Slide Month and Year
			
			var ob = this;
			
			window.requestAnimationFrame(
				function()
				{
					var spanOfSpans = ob.tableDisplay.previousElementSibling.children[1];
					var nextSpan = spanOfSpans.querySelector("span[data-yearmonth='" + ob.selectedDate.getFullYear() + "" + ob.selectedDate.getMonthh() + "']");
					var activeSpan = spanOfSpans.querySelector("span.active");
					//this.tableDisplay.previousElementSibling.children[1].innerHTML = this.monthNames[this.selectedDate.getMonth()] + " " + this.selectedDate.getFullYear();
					
					// Slide Calendar
					var activeTable = ob.tableDisplay.querySelector("table.active");
					var nextTable = ob.tableDisplay.querySelector("table[data-yearmonth='" + ob.selectedDate.getFullYear() + "" + ob.selectedDate.getMonthh() + "']");
					
					if(activeTable != nextTable)
					{
						if(!nextTable)
						{
							nextTable = ob.createTable();
							nextSpan = spanOfSpans.querySelector("span[data-yearmonth='" + ob.selectedDate.getFullYear() + "" + ob.selectedDate.getMonthh() + "']");
						}
						
						window.requestAnimationFrame(
							function()
							{
								var activeYearMonth = parseInt(activeTable.dataset['yearmonth']);
								var nextYearMonth = parseInt(nextTable.dataset['yearmonth']);
								if(activeYearMonth > nextYearMonth)
								{
									// Switch to Previous Month, so actual becomes next
									activeSpan.classList.remove("active");
									activeSpan.classList.add("next");
									activeTable.classList.remove("active");
									activeTable.classList.add("next");
									
									// and previous table becomes actual
									nextSpan.classList.remove("previous");
									nextSpan.classList.add("active");
									nextTable.classList.remove("previous");
									nextTable.classList.add("active");
								}
								else
								{
									// Next Month
									// Switch to Next Month, so actual becomes previous
									activeSpan.classList.remove("active");
									activeSpan.classList.add("previous");
									activeTable.classList.remove("active");
									activeTable.classList.add("previous");
									
									// and next table becomes actual
									nextSpan.classList.remove("next");
									nextSpan.classList.add("active");
									nextTable.classList.remove("next");
									nextTable.classList.add("active");
								}
							}
						);
					}
					
					var selectedDay = ob.tableDisplay.querySelector("td.selected");
					var shouldSelected = ob.tableDisplay.querySelector([
						"table[data-yearmonth='",
						ob.selectedDate.getFullYear(),
						ob.selectedDate.getMonthh(),
						"'] ",
						"td[data-year='",
						ob.selectedDate.getFullYear(),
						"'][data-month='",
						ob.selectedDate.getMonth(),
						"'][data-day='",
						ob.selectedDate.getDate(),
						"']"
					].join(""));
					
					if(selectedDay != shouldSelected && shouldSelected)
					{
						selectedDay.classList.remove("selected");
						shouldSelected.classList.add("selected");
					} // else, no change
					
					var actualDay = ob.tableDisplay.querySelector("td.actual");
					var shouldActual = ob.tableDisplay.querySelector([
						"table[data-yearmonth='",
						ob.actualDate.getFullYear(),
						ob.actualDate.getMonthh(),
						"'] ",
						"td[data-year='",
						ob.actualDate.getFullYear(),
						"'][data-month='",
						ob.actualDate.getMonth(),
						"'][data-day='",
						ob.actualDate.getDate(),
						"']"
					].join(""));
					
					if(actualDay != shouldActual)
					{
						actualDay.classList.remove("actual");
						shouldActual.classList.add("actual");
					} // else, no change
				}
			);
		}
	;
	
	this.updateDisplay = function()
		{
			if(this.display)
			{
				var ob = this;
				var disp = "";
				disp += this.dayNames[this.actualDate.getDDay()];
				disp += ", ";
				disp += this.actualDate.getDate();
				disp += " ";
				disp += this.monthNames[this.actualDate.getMonth()];
				disp += " ";
				disp += this.actualDate.getFullYear();
				disp += "r.";
				this.display.innerHTML = disp;
			}
		}
	;
}

function Note(y, m, d, text)
{
	this.year = y;
	this.month = m;
	this.day = d;
	this.text = text;
	this.addedDate = new Date();
}