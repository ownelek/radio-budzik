﻿function _Clock(display, analogClock, date)
{
	this.date = date ? date : new Date();
	this.activeEditing = false;
	this.lastValue = null;
	this.display = display;
	this.analogDisplay = analogClock;
	this.classes = { hours: 24, minutes: 60, seconds: 60 };
	this.padField = function(val)
		{
			return val < 10 ? "0" + val : val;
		}
	;
	this.init = function()
		{
			var ob = this;
			var classes = this.classes.keys();
			for(var i = 0; i < 3; i++)
			{
				if(i > 0)
				{
					this.display.appendChild(document.createTextNode(":"));
				}
				
				var span = document.createElement("div");
				span.className = classes[i];
				var spanFirst = document.createElement("div");
				spanFirst.className = "first";
				var spanSecond = document.createElement("div");
				spanSecond.className = "second";
				
				for(var j = 0; j < 10; j++)
				{
					var num = document.createElement("div");
					num.className = "num" + j;
					num.appendChild(document.createTextNode(j));
					
					var secNum = num.cloneNode();
					secNum.appendChild(document.createTextNode(j));
					spanFirst.appendChild(num);
					
					spanSecond.appendChild(secNum);
				}
				
				span.appendChild(spanFirst);
				span.appendChild(spanSecond);
				this.display.appendChild(span);
			}
			
			this.display.ondblclick = function()
				{
					var box = new AlertBox("Zmiana godziny", "", [
						{
							text: "Anuluj",
							onclick: function()
								{
									clearInterval(box.interval);
									box.close();
								}
						},
						{
							text: "Zatwierdź",
							onclick: function()
								{
									ob.date.setHours(parseInt(this.form.hours.value));
									ob.date.setMinutes(parseInt(this.form.minutes.value));
									ob.date.setSeconds(parseInt(this.form.seconds.value));
									clearInterval(box.interval);
									box.close();
								}
						}
					]);
					var table = document.createElement("table");
					table.id = "timeEdit";
					table.cellSpacing = "0";
					
					var headRow = document.createElement("tr");
					var headHour = document.createElement("td");
					headHour.appendChild(document.createTextNode("Godzina"));
					var headMinute = document.createElement("td");
					headMinute.appendChild(document.createTextNode("Minuta"));
					var headSecond = document.createElement("td");
					headSecond.appendChild(document.createTextNode("Sekunda"));
					headRow.appendChild(headHour);
					headRow.appendChild(headMinute);
					headRow.appendChild(headSecond);
					table.appendChild(headRow);
					
					var valueRow = document.createElement("tr");
					
					for(var key in ob.classes)
					{
						var value = document.createElement("td");
						var editable = document.createElement("div");
						editable.contentEditable = true;
						editable.spellCheck = false;
						editable.originalValue = ob.date["get" + key.capitalize()]();
						editable.textContent = ob.padField(editable.originalValue);
						editable.classList.add(key);
						editable.maxValue = ob.classes[key];
						editable.notChanged = true;
						editable.onkeydown = function(event)
							{
								// Backspace, Tab, ArrowLeft, ArrowRight, Delete, Numbers 0-9
								var allowed = [ 8, 9, 37, 39, 46, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57 ];
								if(allowed.indexOf(event.keyCode) == -1)
									return false;
								
								// More 2 chars? Lol nop
								if(this.textContent.length >= 2 && !isNaN(parseInt(event.key)))
								{
									return false;
								}
							}
						;
						
						editable.onkeyup = function(event)
							{
								var val = parseInt(this.textContent);
								if(val != this.originalValue)
									this.notChanged = false;
								
								//console.log(key);
								if(val > this.maxValue)
								{
									this.textContent = ob.padField(val - Math.floor(val / this.maxValue) * this.maxValue);
								}
								
								this.nextElementSibling.value = this.textContent; // Should be input[type=hidden] 
							}
						;
						
						var input = document.createElement("input");
						input.name = key;
						input.type = "hidden";
						input.value = ob.date["get" + key.capitalize()]();
						
						value.appendChild(editable);
						value.appendChild(input);
						valueRow.appendChild(value);
					}
					
					table.appendChild(valueRow);
					
					box.interval = setInterval(
						function()
						{
							for(var i = 0; i < valueRow.children.length; i++)
							{
								var div = valueRow.children[i].firstChild;
								if(document.activeElement != div && div.notChanged)
								{
									div.originalValue = ob.date["get" + div.className.capitalize()]();
									div.textContent = ob.padField(div.originalValue);
									div.nextElementSibling.value = div.textContent;
								}
							}
						}, 1000
					);
					box.messageElement.appendChild(table);
					box.formBox();
					box.display("center", "center", true);
					
					return false;
				}
			;
			
			// ---------------------
			// Create Analog Clock
			// ---------------------
			// Pointers
			var analogDiv = document.createElement("div");
			var hP = document.createElement("div");
			hP.id = "hourPointer";
			var mP = document.createElement("div");
			mP.id = "minutePointer";
			var sP = document.createElement("div");
			sP.id = "secondPointer";
			analogDiv.appendChild(hP);
			analogDiv.appendChild(mP);
			analogDiv.appendChild(sP);
			
			// Numbers
			for(var i = 1; i <= 12; i++)
			{
				var outerSpan = document.createElement("span");
				outerSpan.classList.add("number");
				outerSpan.style.transform = "rotate(" + (i * 30) + "deg)";
				var innerSpan = document.createElement("span");
				innerSpan.style.transform = "rotate(-" + (i * 30) + "deg)";
				innerSpan.appendChild(document.createTextNode(i));
				outerSpan.appendChild(innerSpan);
				analogDiv.appendChild(outerSpan);
			}
			
			// Seconds Indicators
			for(var i = 0; i < 60; i++)
			{
				var deg = i * 6;
				if(deg % 30 != 0)
				{
					var indicator = document.createElement("span");
					indicator.classList.add("indicator");
					indicator.style.transform = "rotate(" + deg + "deg)";
					indicator.appendChild(document.createElement("span"));
					analogDiv.appendChild(indicator);
				}
			}
			
			this.analogDisplay.appendChild(analogDiv);
			
			this.updateDisplay();
			this.updateAnalog();
			setInterval(
				function() { ob.nextSecond(); }, 1000
			);
		}
	;
	
	this.nextSecond = function()
		{
			this.date.setSeconds(this.date.getSeconds() + 1);
			// Check whether we have summer or winter time
			document.querySelector("#rodzajczasu span").innerHTML = this.date.getTimezoneOffset() == -60 ? "zimowy".bold() : "letni".bold();
			this.updateDisplay();
			this.updateAnalog();
		}
	;
	
	this.updateDisplay = function()
		{
			var querySelector = this.display.querySelector.bind(this.display);
			var firstTo = {};
			var secondTo = {};
			for(var cls in this.classes)
			{
				var time = this.date[ "get" + cls.capitalize() ]();
				var first = Math.floor(time / 10);
				var second = time - first * 10;
				
				// First number
				var actual = querySelector("." + cls + " .first .actual");
				var next = querySelector("." + cls + " .first .num" + first);
				if(next != actual)
				{
					if(actual)
					{
						actual.classList.remove("actual");
						actual.classList.add("down");
						firstTo[cls] = actual;
					}
					
					next.classList.add("rotateDown");
					next.classList.add("actual");
				}
				
				// Second number
				actual = querySelector("." + cls + " .second .actual");
				next = querySelector("." + cls + " .second .num" + second);
				
				if(next != actual)
				{
					if(actual)
					{
						actual.classList.remove("actual");
						actual.classList.add("down");
						secondTo[cls] = actual;
					}
					
					next.classList.add("rotateDown");
					next.classList.add("actual");
				}
			}
			
			setTimeout(
				function()
				{
					for(var key in firstTo)
					{
						firstTo[key].classList.remove("rotateDown");
						firstTo[key].classList.remove("down");
					}
					
					for(var key in secondTo)
					{
						secondTo[key].classList.remove("rotateDown");
						secondTo[key].classList.remove("down");
					}
				}, 510
			);
		}
	;
	
	this.updateAnalog = function()
		{
			var hourPointer = this.analogDisplay.querySelector("#hourPointer");
			var minutePointer = this.analogDisplay.querySelector("#minutePointer");
			var secondPointer = this.analogDisplay.querySelector("#secondPointer");
			
			var rotation = 180; // starts with 180 coz html is stupid
			var hourDegree = 30; // 30 degrees for every number
			var secondDegree = 6; // wut, so low ;_;
			
			var hour = this.date.getHours();
			var minute = this.date.getMinutes();
			var second = this.date.getSeconds();
			var nextHourPerc = (minute * 60 + second) / 3600;
			var nextMinutePerc = second / 60;
			
			hourPointer.style.transform = "rotate(" + ((hour > 11 ? (hour - 12) : hour) * hourDegree + nextHourPerc * hourDegree - rotation) + "deg)";
			minutePointer.style.transform = "rotate(" + (minute * secondDegree + nextMinutePerc * secondDegree - rotation) + "deg)";
			secondPointer.style.transform = "rotate(" + (second * secondDegree - rotation) + "deg)";
		}
	;
}