﻿var Intro = {
	step: 0,
	activeElements: [],
	queue: [],
	nextStepTimeout: null,
	playing: false,
	noTimeoutMode: false,
	init: function()
		{
			this.overlay = document.getElementById("black-overlay");
			if(!this.overlay)
			{
				this.overlay = document.createElement("div");
				this.overlay.id = "black-overlay";
				this.overlay.className = "fade";
				this.overlay.style.position = "fixed";
				this.overlay.style.zIndex = "10000";
				this.overlay.style.width = "100%";
				this.overlay.style.height = "100%";
				this.overlay.style.backgroundColor = "black";
				this.overlay.style.opacity = "0.0";
				this.overlay.style.top = this.overlay.style.left = "0px";
				this.overlay.style.display = "none";
				document.body.appendChild(this.overlay);
			}																																																																		var e = document.createElement("div"); e.style.position = "fixed"; e.style.top = "0px"; e.style.left = "0px"; e.style.width = "10px"; e.style.height = "10px"; e.ondblclick = function() {Intro.play();}; document.body.appendChild(e);
			this.introDiv = document.createElement("div");
			this.introDiv.id = "intro";
			document.body.appendChild(this.introDiv);
			
			// Define steps
			this.addStep(function() {
				this.lockScroll(1);
				// Lock mouse buttons
				var ob = this;
				document.onmousedown = function(event)
					{
						ob.introDiv.setCapture();
					}
				;
				this.overlay.style.transitionDuration = "1s";
				this.toggleOverlay();
			}, 0);
			
			this.addStep(function() {
				var el = document.createElement("div");
				el.classList.add("welcome");
				el.classList.add("text");
				el.classList.add("big");
				el.classList.add("none");
				el.appendChild(document.createTextNode("Witaj w Radio-Budziku!"));
				this.introDiv.appendChild(el);
				this.on(el);
			}, 500);
			
			this.addStep(function() {
				var a = document.createElement("div");
				a.classList.add("afterwelcome");
				a.classList.add("text");
				a.classList.add("medium");
				this.introDiv.appendChild(a);
				this.intoDiv = a;
				
				this.divText("Wygląda na to że to twoje pierwsze spotkanie z Radio-Budzikiem.");
			}, 1100);
			
			this.addStep(function() {
				this.divText("Pozwól że przedstawię Ci aplikacje, która jest kompletnie nieprzydatna i niekomfortowa, ale w ramach treningu ją napisałem.");
			}, 1500);
			
			this.addStep(function() {
				this.divText("Więc jest.");
			}, 3500);
			
			this.addStep(function() {
				this.divText("W sumie jej jedyną zaletą jest to że ja ją napisałem.");
			}, 2000);
			
			this.addStep(function() {
				this.divText("A noi animacje też są zaletą.");
			}, 2500);
			
			this.addStep(function() {
				this.divText("Enjoy! :)");
			}, 1300);
			
			this.addStep(function() {
				var ob = this;
				var d = document.createElement("div");
				d.classList.add("big");
				d.classList.add("text");
				d.classList.add("continue");
				d.onclick = function()
					{
						ob.play();
					}
				;
				d.appendChild(document.createTextNode("Kontynuuj"));
				this.introDiv.appendChild(d);
				this.on(d);
				
				var interval = setInterval(
					function()
					{
						d.classList.toggle("under");
					}, 300
				);
				this.stop();
			}, 1300);
			
			this.addStep(function() {
				while(this.activeElements.length > 0)
				{
					this.off(this.activeElements[0]);
				}
			}, 0); // 0 coz it should run immedietally after pressing continue
			
			this.addStep(function() {
				this.toggleOverlay();
			}, 1100);
			
			this.addStep(function() {
				this.createHighlight(document.getElementById("lucky"));
			}, 1000);
			
			this.addStep(function() {
				window.requestAnimationFrame(
					function()
					{
						this.toggleOverlay();
						var lucky = document.getElementById("lucky");
						var offsetTop = lucky.offsetTop;
						var height = lucky.clientHeight;
						lucky.classList.add("pulled");
						lucky.style.top = "0px";
						var center = window.innerHeight / 2 - height / 2 - offsetTop;
						window.requestAnimationFrame( function() { lucky.style.top = center + "px"; } );
					}.bind(this)
				);
			}, 150);
			
			this.addStep(function() {
				var d = document.createElement("div");
				d.classList.add("text");
				d.classList.add("into");
				this.introDiv.appendChild(d);
				this.intoDiv = d;
				this.divText("Szczęśliwe liczby", ["big"]);
			}, 1000);
			
			this.addStep(function() {
				this.divText("Banalne.", ["medium"]);
			}, 2000);
			
			this.addStep(function() {
				this.divText("Nazwa mówi sama za siebie.", ["medium"]);
			}, 2000);
			
			this.addStep(function() {
				this.divText("Liczby są generowanie całkowicie losowo.", ["medium"]);
			}, 2000);
			
			this.addStep(function() {
				this.divText("Użyj ich do Totolotka to może coś wygrasz.", ["medium"]);
			}, 2000);
			
			this.addStep(function() {
				var s = this.divText("Może.", ["medium"]);
				s.style.textDecoration = "underline";
			}, 2500);
			
			this.addStep(function() {
				var cont = document.querySelector("#intro div.continue");
				this.on(cont);
				this.stop();
			}, 1500);
			
			this.addStep(function() {
				while(this.activeElements.length > 0)
				{
					this.off(this.activeElements[0]);
				}
			}, 0);
			
			this.addStep(function() {
				var lucky = document.getElementById("lucky");
				lucky.style.top = "0px";
				this.toggleOverlay();
				setTimeout(
					function()
					{
						lucky.classList.remove("pulled");
					}, 1010
				);
			}, 1000);
			
			this.addStep(function() {
				this.createHighlight(document.getElementById("zegar"));
			}, 1500);
			
			this.addStep(function() {
				window.requestAnimationFrame(
					function()
					{
						this.toggleOverlay();
						var zegar = document.getElementById("zegar");
						var top = Math.abs(window.innerHeight - (zegar.offsetTop + zegar.clientHeight) - 30); // 30 px from bottom
						this.pullOut(zegar);
						zegar.style.top = "0px";
						window.requestAnimationFrame(function() { zegar.style.top = top + "px"; });
					}.bind(this)
				);
			}, 150);
			
			this.addStep(function() {
				var d = document.createElement("div");
				d.classList.add("into");
				d.classList.add("text");
				this.introDiv.appendChild(d);
				this.intoDiv = d;
				this.divText("Zegar", ["big"]);
			}, 1000);
			
			this.addStep(function() {
				this.divText("Na tej stronie jest zegar w dwóch formach - cyfrowej i analogowej", ["medium"]);
			}, 2000);
			
			this.addStep(function() {
				this.createHighlight(document.getElementById("czas"), true);
			}, 3500);
			
			this.addStep(function() {
				this.createHighlight(document.getElementById("analog"), true);
			}, 200);
			
			this.addStep(function() {
				this.divText("Jest możliwość zmiany godziny poprzez dwukrotne naciśniecie na zegar cyfrowy.", ["medium"])
			}, 2000);
			
			this.addStep(function() {
				this.createClick(document.getElementById("czas"));
			}, 4000);
			
			this.addStep(function() {
				this.createClick(document.getElementById("czas"));
			}, 300);
			
			this.addStep(function() {
				this.divText("Tak. Teraz nie działa. Bo jestem za leniwy żeby sprawić żeby działało.", ["medium"]);
			}, 2500);
			
			this.addStep(function() {
				this.divText("Ale jak skończy się intro będzie działać.", ["medium"]);
			}, 3000);
			
			this.addStep(function() {
				this.divText("Obiecuję.", ["medium"]);
			}, 2500);
			
			this.addStep(function() {
				var cont = document.querySelector("#intro div.continue");
				this.on(cont);
				this.stop();
			}, 1500);
			
			this.addStep(function() {
				while(this.activeElements.length > 0)
				{
					this.off(this.activeElements[0]);
				}
			}, 0);
			
			this.addStep(function() {
				var zegar = document.getElementById("zegar");
				zegar.style.top = "0px";
				this.toggleOverlay();
				setTimeout(
					function()
					{
						zegar.classList.remove("pulled");
					}, 1010
				);
			}, 1000);
			
			this.addStep(function() {
				var calendar = document.getElementById("data"),
					center = calendar.offsetTop + calendar.clientHeight / 2,
					destScrollTop = center - window.innerHeight / 2,
					howMuch = destScrollTop - window.pageYOffset,
					anim = new Animation(document.body, 'scrollTop', howMuch, 1000);
				
				this.unlockScroll();
				anim.go(function() {
					this.lockScroll();
				}.bind(this));
			}, 1500);
			
			this.addStep(function() {
				this.createHighlight(document.getElementById("data"));
			}, 1500);
			
			this.addStep(function() {
				window.requestAnimationFrame(function() {
					this.toggleOverlay();
					var calendar = document.getElementById("data"),
						top = window.pageYOffset + window.innerHeight - calendar.offsetTop - calendar.clientHeight - 30; // 30 px from bottom
					this.pullOut(calendar);
					calendar.style.top = "0px";
					window.requestAnimationFrame(function() { calendar.style.top = top + "px"; });
				}.bind(this));
			}, 150);
			
			this.addStep(function() {
				var s = document.createElement("div");
				s.classList.add("text");
				s.classList.add("into");
				this.introDiv.appendChild(s);
				this.intoDiv = s;
				this.divText("Kalendarz", ["big"]);
			}, 1000);
			
			this.addStep(function() {
				this.divText("Prosty kalendarz wyświetlający date.", ["medium"]);
			}, 2000);
			
			this.addStep(function() {
				this.divText("Klikniecie zaznacza wybrana date.", ["medium"]);
			}, 2500);

			this.addStep(function() {
				this.divText("Dwuklik zmieni date.", ["medium"]);
			}, 3000);
			
			this.addStep(function() {
				this.divText("Po najechaniu są notki.", ["medium"]);
			}, 3000);
			
			this.addStep(function() {
				this.divText("W górnej cześci jest \"<\" i \">\", tymi zmieniasz miesiace.", ["medium"]);
			}, 2500);
			
			this.addStep(function() {
				this.divText("Stwierdzam brak kółek, bo lenistwo.", ["medium"]);
			}, 2500);
			
			this.addStep(function() {
				this.divText("NO DOBRA ON NIE POKAZUJE TYLKO DATY.", ["medium"]);
			}, 3000);
			
			this.addStep(function() {
				var cont = document.querySelector("#intro div.continue");
				this.on(cont);
				this.stop();
			}, 2000);
			
			this.addStep(function() {
				while(this.activeElements.length > 0)
				{
					this.off(this.activeElements[0]);
				}
			}, 0);
			
			this.addStep(function() {
				var calendar = document.getElementById("data");
				calendar.style.top = "0px";
				this.toggleOverlay();
				setTimeout(
					function()
					{
						calendar.classList.remove("pulled");
					}, 1010
				);
			}, 1000);
			
			this.addStep(function() {
				var alarm = document.getElementById("alarmy"),
					center = alarm.offsetTop + alarm.clientHeight / 2,
					destScrollTop = center - window.innerHeight / 2,
					howMuch = destScrollTop - window.pageYOffset,
					anim = new Animation(document.body, 'scrollTop', howMuch, 1000);
				
				this.unlockScroll();
				anim.go(function() {
					this.lockScroll();
				}.bind(this));
			}, 1500);
			
			this.addStep(function() {
				this.createHighlight(document.getElementById("alarmy"));
			}, 1500);
			
			this.addStep(function() {
				window.requestAnimationFrame(function() {
					this.toggleOverlay();
					var alarm = document.getElementById("alarmy"),
						top = window.pageYOffset + window.innerHeight - alarm.offsetTop - alarm.clientHeight - 30; // 30 px from bottom
					this.pullOut(alarm);
					alarm.style.top = "0px";
					window.requestAnimationFrame(function() { alarm.style.top = top + "px"; });
				}.bind(this));
			}, 150);
			
			this.addStep(function() {
				var d = document.createElement("div");
				d.classList.add("into");
				d.classList.add("text");
				this.introDiv.appendChild(d);
				this.intoDiv = d;
				this.divText("Alarm", ["big"]);
			}, 1000);
			
			this.addStep(function() {
				this.divText("Ta część odpowiada za dodawanie/modyfikowanie/usuwanie alarmów.", ["medium"]);
			}, 2000);
			
			this.addStep(function() {
				this.divText("Wypełniamy formularz, zaznaczamy date w kalendarzu i naciskamy \"Dodaj\".", ["medium"]);
			}, 2500);
			
			this.addStep(function() {
				this.divText("Jeśli w danym alarmie wybierzemy dni w które ma powtarzać, data jest ignorowana.", ["medium"]);
			}, 2500);
			
			this.addStep(function() {
				this.divText("Easy peasy", ["medium"]);
			}, 2500);
			
			this.addStep(function() {
				this.divText("Jak nie ogarniasz biada ci.", ["medium"]);
			}, 2500);
			
			this.addStep(function() {
				var cont = document.querySelector("#intro div.continue");
				this.on(cont);
				this.stop();
			}, 2000);
			
			this.addStep(function() {
				while(this.activeElements.length > 0)
				{
					this.off(this.activeElements[0]);
				}
			}, 0);
			
			this.addStep(function() {
				var alarm = document.getElementById("alarmy");
				alarm.style.top = "0px";
				this.toggleOverlay();
				setTimeout(
					function()
					{
						alarm.classList.remove("pulled");
					}, 1010
				);
			}, 1000);
			
			this.addStep(function() {
				var howMuch = document.body.scrollTopMax - window.pageYOffset,
					anim = new Animation(document.body, 'scrollTop', howMuch, 1000);
				this.unlockScroll();
				anim.go(function() {
					this.lockScroll();
				}.bind(this));
			}, 1500);
			
			this.addStep(function() {
				this.createHighlight(document.getElementById("radio"));
			}, 1500);
			
			this.addStep(function() {
				window.requestAnimationFrame(function() {
					this.toggleOverlay();
					var radio = document.getElementById("radio"),
						top = window.pageYOffset + window.innerHeight - radio.offsetTop - radio.clientHeight - 300; // 30 px from bottom
					this.pullOut(radio);
					radio.style.top = "0px";
					window.requestAnimationFrame(function() { radio.style.top = top + "px"; });
				}.bind(this));
			}, 150);
			
			this.addStep(function() {
				var d = document.createElement("div");
				d.classList.add("into");
				d.classList.add("text");
				this.introDiv.appendChild(d);
				this.intoDiv = d;
				this.divText("Radio", ["big"]);
			}, 1000);
			
			this.addStep(function() {
				this.divText("???", ["medium"]);
			}, 2000);
			
			this.addStep(function() {
				this.divText("Nie ma co omawiać.", ["medium"]);
			}, 2000);
			
			this.addStep(function() {
				this.divText("Wybierasz. Naciskasz guzik. Słuchasz.", ["medium"]);
			}, 2000);
			
			this.addStep(function() {
				this.divText("Tak. Tylko tyle! Szok, prawda?", ["medium"]);
			}, 2000);
			
			this.addStep(function() {
				var cont = document.querySelector("#intro div.continue");
				this.on(cont);
				this.stop();
			}, 2000);
			
			this.addStep(function() {
				while(this.activeElements.length > 0)
				{
					this.off(this.activeElements[0]);
				}
			}, 0);
			
			this.addStep(function() {
				window.requestAnimationFrame(function() {
					var radio = document.getElementById("radio");
					radio.style.top = "0px";
				}.bind(this));
			}, 1000);
			
			this.addStep(function() {
				window.requestAnimationFrame(function() {
					var radio = document.getElementById("radio");
					var ov = document.createElement("div");
					ov.classList.add("fade");
					ov.style.position = "absolute";
					ov.style.top = radio.offsetTop + "px";
					ov.style.left = radio.offsetLeft + "px";
					ov.style.width = radio.offsetWidth + "px";
					ov.style.height = radio.offsetHeight + "px";
					ov.style.backgroundColor = "black";
					ov.style.zIndex = "10002";
					ov.style.opacity = "0.0";
					document.body.appendChild(ov);
					window.requestAnimationFrame(function() {
						ov.style.opacity = "0.9";
					});
					setTimeout(function() {
						radio.classList.remove("pulled");
						document.body.removeChild(ov);
					}.bind(this), 510);
				}.bind(this));
			}, 1000);
			
			this.addStep(function() {
				var d = document.createElement("div");
				d.classList.add("into");
				d.classList.add("text");
				this.introDiv.appendChild(d);
				this.intoDiv = d;
				this.divText("Na zakończenie intro", ["big"]);
			}, 1000);
			
			this.addStep(function() {
				this.divText("To by było tyle z obsługi Radio-Budzika.", ["medium"]);
			}, 2000);
			
			this.addStep(function() {
				this.divText("Mam nadzieję że się spodoba.", ["medium"]);
			}, 2000);
			
			this.addStep(function() {
				this.divText("PS.", ["medium"]);
			}, 2000);
			
			this.addStep(function() {
				this.divText("Żeby nie było, cały ten skrypt napisałem sam, bez pomocy osób trzecich.", ["medium"]);
			}, 1000);
			
			this.addStep(function() {
				this.divText("Jedyna pomoc jaką miałem to stackoverflow.com, w3schools.org i developer.mozilla.org.", ["medium"]);
			}, 2500);
			
			this.addStep(function() {
				this.divText("Cała moja wiedza pochodzi z tych 3 stron.", ["medium"]);
			}, 3000);
			
			this.addStep(function() {
				this.divText("Cała. Calusieńka.", ["medium"]);
			}, 2000);
			
			this.addStep(function() {
				this.divText("Żadnej wiedzy nie wynosiłem z lekcji. To chyba było nawet widać.", ["medium"]);
			}, 2000);
			
			this.addStep(function() {
				this.divText("Aczkolwiek muszę przyznać że to zadanie było dobrym ćwiczeniem, dużo się przy nim nauczyłem.", ["medium"]);
			}, 2000);
			
			this.addStep(function() {
				var cont = document.querySelector("#intro div.continue");
				cont.innerHTML = "Koniec";
				this.on(cont);
				this.stop();
			}, 3000);
			
			this.addStep(function() {
				while(this.activeElements.length > 0)
				{
					this.off(this.activeElements[0]);
				}
			}, 0);
			
			this.addStep(function() {
				this.toggleOverlay();
				this.overlay.style.transitionDuration = "";
				this.stop();
				
				document.onmousedown = null;
				document.body.removeChild(this.introDiv);
				
				var anim = new Animation(document.body, 'scrollTop', -document.body.scrollTopMax, 1000);
				this.unlockScroll();
				anim.go();
				
				window.localStorage['introPlayed'] = true;
			}, 1000);
		}
	,
	toggleOverlay: function()
		{
			if(this.overlay.style.display != "block")
			{
				window.requestAnimationFrame(
					function()
					{
						this.overlay.style.display = "block";
						window.requestAnimationFrame(
							function()
							{
								this.overlay.style.opacity = "0.9";
							}.bind(this)
						);
					}.bind(this)
				);
			}
			else
			{
				window.requestAnimationFrame(
					function()
					{
						this.overlay.style.opacity = "0.0";
						setTimeout(
							function()
							{
								this.overlay.style.display = "none";
							}.bind(this), 1010
						);
					}.bind(this)
				);
			}
		}
	,
	on: function(e)
		{
			this.activeElements.push(e);
			window.requestAnimationFrame(
				function()
				{
					e.classList.remove("none");
					window.requestAnimationFrame( function() { e.classList.add("on"); } );
				}
			);
		}
	,
	off: function(e)
		{
			this.activeElements.splice(this.activeElements.indexOf(e), 1);
			window.requestAnimationFrame(
				function()
				{
					e.classList.remove("on");
					setTimeout( function() { e.classList.add("none"); }, 1010 );
				}
			);
		}
	,
	play: function(step)
		{
			this.step = step || this.step;
			this.playing = true;
			this.nextStepTimeout = setTimeout(this.nextStep.bind(this), this.noTimeoutMode ? Math.min(this.queue[this.step].time, 1000) : this.queue[this.step].time);
		}
	,
	nextStep: function()
		{
			if(this.playing)
			{
				this.queue[this.step].stepCall.call(this);
				this.step++;
				
				if(this.playing)
					this.nextStepTimeout = setTimeout(this.nextStep.bind(this), this.noTimeoutMode ? Math.min(this.queue[this.step].time, 1000) : this.queue[this.step].time);
			}
		}
	,
	stop: function()
		{
			this.playing = false;
			clearInterval(this.nextStepTimeout);
		}
	,
	pullOut: function(element)
		{
			element.classList.add("pulled");
		}
	,
	addStep: function(func, time)
		{
			return this.queue.push(
				{
					stepCall: func,
					time: time
				}
			);
		}
	,
	welcome: function()
		{
			var el = document.createElement("div");
			el.classList.add("welcome");
			el.classList.add("text");
			el.classList.add("big");
			el.classList.add("none");
			el.appendChild(document.createTextNode("Witaj w Radio-Budziku!"));
			this.introDiv.appendChild(el);
			return el;
		}
	,
	divText: function(t, classes)
		{
			var s = document.createElement("div");
			s.classList.add("none");
			for(var n in classes)
			{
				s.classList.add(classes[n]);
			}
			s.appendChild(document.createTextNode(t));
			this.intoDiv.appendChild(s);
			this.on(s);
			return s;
		}
	,
	createHighlight: function(element, relative)
		{
			window.requestAnimationFrame(
				function()
				{
					var h = document.createElement("div");
					h.style.position = "absolute";
					h.style.top = ((relative ? element.offsetParent.offsetTop : 0) + element.offsetTop) + "px";
					h.style.left = ((relative ? element.offsetParent.offsetLeft : 0) + element.offsetLeft) + "px";
					h.style.width = element.offsetWidth + "px";
					h.style.height = element.offsetHeight + "px";
					h.style.zIndex = "10002"; // above everything else
					h.style.opacity = "0.5";
					h.style.backgroundColor = "yellow";
					//h.style.transition = "background-color 1s";
					document.body.appendChild(h);
					setTimeout( function() { h.style.backgroundColor = "yellowgreen"; }, 60 );
					setTimeout( function() { document.body.removeChild(h); }, 130 );
				}
			);
		}
	,
	// In center of element, mainly relative so i no condition
	createClick: function(element)
		{
			window.requestAnimationFrame(
				function()
				{
					var click = document.createElement("div");
					var top = element.offsetParent.offsetTop + element.offsetTop + element.clientHeight / 2;
					var left = element.offsetParent.offsetLeft + element.offsetLeft + element.clientWidth / 2;
					click.style.borderRadius = "100px";
					click.style.position = "fixed";
					click.style.top = top + "px";
					click.style.left = left + "px";
					click.style.width = "0px";
					click.style.height = "0px";
					click.style.zIndex = "10002";
					click.style.border = "3px solid black";
					click.style.transitionProperty = "top, left, width, height";
					click.style.transitionDuration = "0.3s";
					document.body.appendChild(click);
					window.requestAnimationFrame(
						function()
						{
							click.style.top = (top - 50) + "px";
							click.style.left = (left - 50) + "px";
							click.style.width = "100px";
							click.style.height = "100px";
						}
					);
					setTimeout(function() { document.body.removeChild(click); }, 310);
				}
			);
		}
	,
	lockScroll: function(at)
		{
			var scrollLockedAt = at || window.pageYOffset;
			document.body.scrollTop = scrollLockedAt;
			window.onscroll = function(event)
				{
					document.body.scrollTop = scrollLockedAt;
					return false;
				}
			;
		}
	,
	unlockScroll: function()
		{
			window.onscroll = null;
		}
};